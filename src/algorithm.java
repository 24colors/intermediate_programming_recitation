import java.util.Scanner;

public class algorithm {

	public static void main(String[] args) {
		int a, b;
		Scanner sc = new Scanner (System.in);
		System.out.println("give 2 positive numbers");
		System.out.println("The GCD of the two numbers is" + " " + GCD(sc.nextInt(), sc.nextInt()));
		sc.nextLine();
		
	}
	public static int GCD (int a,int b) {
//return a as the GCD if the b is equal to 0
//if it is not equal to 0, go to else statement
		if (b == 0)
			return a;
		else
//if b is not equal to 0, it will return the value for GCD
//stores the value of b and will return as a value of GCD if the remainder of a%b will equal to 0
//if the remainder is not equal to 0 it will become the new value of b
//the value of b that is given by the user will become the new value of a and will do statement (b, a%b)
			return GCD(b, a%b);
 		
	}
}